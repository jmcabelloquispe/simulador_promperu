var main = (function() {
		
	var st = {
		content: ".js-content",
		simulator: ".js-simulator",
		home: ".js-home",
		file: ".js-inputfile",
		modal_open: ".js-modal-open",
		modal_close: ".js-modal-close",
		dt_listtag: ".js-dt-listtag",
		option_label: ".js-option-label",
		user_type: ".js-user-type",
		inputs_user_natural: ".js-user-natural",
		inputs_user_legal: ".js-user-legal",
		info_open: ".js-info-open",
		modal_info: ".js-info",
		dt_lissizes: ".js-dt-lissizes"
	};

	var dom = {};

	var catchDom = function() {
		dom.content = $(st.content);	
		dom.simulator = $(st.simulator);	
		dom.home = $(st.home);	
		dom.file = $(st.file);	
		dom.modal_open = $(st.modal_open);	
		dom.modal_close = $(st.modal_close);		
		dom.dt_listtag = $(st.dt_listtag);		
		dom.option_label = $(st.option_label);	
		dom.user_type = $(st.user_type);
		dom.inputs_user_natural = $(st.inputs_user_natural);
		dom.inputs_user_legal = $(st.inputs_user_legal);
		dom.info_open = $(st.info_open);
		dom.modal_info = $(st.modal_info);
		dom.dt_lissizes = $(st.dt_lissizes);
	};

	function funcionality() {

		$(window).resize(function() {
			recalculateHeight();
			recalculateHomeHeight();
		});

		$(document).on('change', st.file, function(e) { 
			try {
				var name = e.target.files[0].name;
			}catch(err){
				var name = "";
			}
			$(this).parent().find(".fake-input").text(name);
		});

		$(dom.modal_close).on("click", function() {
			var modal = "." + $(this).attr("data-modal-close");
			$(modal).css("display", "none");			
			$(dom.option_label).removeClass('option--underline');
			
		});

		$(dom.modal_open).on("click", function() {
			var modal = "." + $(this).attr("data-modal-open");
			$(modal).css("display", "flex")
		});

		$(dom.option_label).on("click", function(){
			$(dom.option_label).removeClass('option--underline');
			$(this).addClass("option--underline");
		});

		$(document).on('click', ".js-info-open", function(event){			
			event.stopPropagation();
			var message =$(this).attr("data-message");
			$(st.modal_info + " .message").text(message);
			$(dom.modal_info).css("display", "flex");
		});

		$(".js-infostep-open").each(function(){
			$(this).parent().before(this);
		});

		$(document).on('click', ".js-infostep-open", function(event){	
			var message =$(this).attr("data-message");
			$(st.modal_info + " .message").text(message);
			$(dom.modal_info).css("display", "flex");
		});

		// muestra y oculta campos por tipo de usuario
		$(dom.user_type).on("change", function(){
			if($(this).val()==='1'){
				$(dom.inputs_user_legal).css("display", "none");
				$(dom.inputs_user_natural).css("display", "block");				
			}else {				
				$(dom.inputs_user_natural).css("display", "none");
				$(dom.inputs_user_legal).css("display", "block");
			}
		});
		
		$(dom.dt_listtag).DataTable({
			"info": false,
			"bLengthChange": false,
			pageLength : 3,
			language: { search: "", searchPlaceholder: "Filtrar"},
		});

		$(dom.dt_lissizes).DataTable({
			"info": false,
			"bLengthChange": false,
			pageLength : 3,
			language: { search: "", searchPlaceholder: "Filtrar"},
		});
	};

	function recalculateHeight() {
		var height =  $(window).height() - ($('header').height()+$('footer').height());
		$(dom.content).css('min-height', height.toString() + 'px')
		$(dom.simulator).css('min-height', height.toString() + 'px')
	}

	function recalculateHomeHeight() {
		var height =  $(window).height();
		$(dom.home).css('min-height', height.toString() + 'px')
	}


	var initialize = function() {
		catchDom();		
		recalculateHeight();
		recalculateHomeHeight();
		funcionality();
	};

	return {
		init: initialize
	};

})();
	
module.exports = main;
	