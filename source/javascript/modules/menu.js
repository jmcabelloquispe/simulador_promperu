var menu = (function() {
		
	var st = {
        user: ".js-user",
        menu_options: ".js-menu-options",
        drink_option: ".js-drink",
        menu_drink_options: ".js-drink-options"
	};

	var dom = {};

	var catchDom = function() {
        dom.user = $(st.user);	
        dom.menu_options = $(st.menu_options);	
        dom.drink_option = $(st.drink_option);	
        dom.menu_drink_options = $(st.menu_drink_options);			
	};

	function funcionality() {

        var showMenuUser = false;
		$(dom.user).on("click", function() {
            if(showMenuUser){
                $(dom.menu_options).css("display", "none");
                showMenuUser = false;
            }else {                
                $(dom.menu_options).css("display", "flex");
                showMenuUser = true;
            }
        });   
        
        var showMenuDrink = false;
		$(dom.drink_option).on("click", function() {
            if(showMenuDrink){
                $(this).removeClass('option--showline')
                $(dom.menu_drink_options).css("display", "none");
                showMenuDrink = false;
            }else {                
                $('.labels .option').removeClass('option--showline')
                $(this).addClass('option--showline')
                $(dom.menu_drink_options).css("display", "flex");
                showMenuDrink = true;
            }
        }); 

        $(document).click(function(event) {
            if (!$(event.target).closest(".js-user, .js-menu-options",st.user).length) {
                $("body").find(dom.menu_options).css("display", "none");
            }
        });      
	};

	var initialize = function() {
        catchDom();
		funcionality();
	};

	return {
		init: initialize
	};

})();
	
module.exports = menu;
	