var shoe = (function() {
		
	var st = {
        form: "#form-shoe",
        modal_armed: ".js-armed",
        button_shoe_add_seals: ".js-shoe-add-seals",
        button_shoe_remove_seals: ".js-shoe-remove-seals",
        button_shoe_add_country: ".js-shoe-add-country",
        button_shoe_remove_country: ".js-shoe-remove-country",
	};

	var dom = {};

	var catchDom = function() {
        dom.form = $(st.form);	
        dom.modal_armed = $(st.modal_armed);	
        dom.button_shoe_add_seals = $(st.button_shoe_add_seals);		
        dom.button_shoe_remove_seals = $(st.button_shoe_remove_seals);		
        dom.button_shoe_add_country = $(st.button_shoe_add_country);    
        dom.button_shoe_remove_country = $(st.button_shoe_remove_country);	
	};

	function funcionality() {
      $(dom.form).steps({
         headerTag: "h3",
         bodyTag: "section",
         transitionEffect: "fade",
         autoFocus: true,
         labels: {
         next: "Siguiente >",
         finish: "Generar >"
         },
         onFinished: function (event, currentIndex)
         {
            $(dom.modal_armed).css("display", "flex");
         }
      });

      $(document).on('click', st.button_shoe_add_seals, function(){
         var seals = $("body").find(".js-shoe-seals-clone").clone();
         seals.removeClass("js-shoe-seals-clone");      
         seals.find(".js-shoe-add-seals").removeClass("js-shoe-add-seals").addClass('js-shoe-remove-seals').text("Eliminar");
         seals.insertAfter(".js-shoe-seals-clone");
      });

      $(document).on('click', ".js-shoe-remove-seals", function(){
         $(this).parent().parent().remove();
      });

      $(document).on('click', st.button_shoe_add_country, function(){
         var country = $("body").find(".js-shoe-country-clone").clone();
         country.removeClass("js-shoe-country-clone");      
         country.find(".js-shoe-add-country").removeClass("js-shoe-add-country").addClass('js-shoe-remove-country').text("Eliminar");
         country.insertAfter(".js-shoe-country-clone");
       });
   
       $(document).on('click', ".js-shoe-remove-country", function(){
         $(this).parent().parent().remove();
       });
	};

	var initialize = function() {
      catchDom();
		funcionality();
	};

	return {
		init: initialize
	};

})();
	
module.exports = shoe;
	