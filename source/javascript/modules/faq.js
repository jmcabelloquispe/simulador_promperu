var faq = (function() {
		
	var st = {
        btn_accordion: "js-btn_accordion",
	};

	var dom = {};

	var catchDom = function() {
        dom.btn_accordion = document.getElementsByClassName(st.btn_accordion);			
	};

	function funcionality() {        
        var i;
        for (i = 0; i < dom.btn_accordion.length; i++) {
            dom.btn_accordion[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight){
                panel.style.maxHeight = null;
                } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
                } 
            });
        }
	};

	var initialize = function() {
        catchDom();
		funcionality();
	};

	return {
		init: initialize
	};

})();
	
module.exports = faq;
	