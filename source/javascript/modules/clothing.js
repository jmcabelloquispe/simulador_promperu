var clothing = (function() {
		
	var st = {
        form: "#form-clothing",
        modal_armed: ".js-armed",
        modal_generatedtag: ".js-generatedtag",
        input_clothing_fiber: ".js-clothing-fiber",
        input_clothing_percentage: ".js-clothing-percentage",
        button_add_fiber: ".js-add-fiber",
        button_remove_fiber: ".js-remove-fiber",
        button_clothing_add_seals: ".js-clothing-add-seals",
        button_clothing_remove_seals: ".js-clothing-remove-seals",
        button_clothing_add_country: ".js-clothing-add-country",
        button_clothing_remove_country: ".js-clothing-remove-country",
	};

	var dom = {};

	var catchDom = function() {
        dom.form = $(st.form);	
        dom.modal_armed = $(st.modal_armed);		
        dom.modal_generatedtag = $(st.modal_generatedtag);
        dom.button_add_fiber = $(st.button_add_fiber);    
        dom.button_remove_fiber = $(st.button_remove_fiber);   
        dom.button_clothing_add_seals = $(st.button_clothing_add_seals);    
        dom.button_clothing_remove_seals = $(st.button_clothing_remove_seals);		
        dom.button_clothing_add_country = $(st.button_clothing_add_country);    
        dom.button_clothing_remove_country = $(st.button_clothing_remove_country);		
	};

	function funcionality() {

    $(dom.form).steps({
      headerTag: "h3",
      bodyTag: "section",
      transitionEffect: "fade",
      autoFocus: true,
      labels: {
      next: "Siguiente >",
      finish: "Generar >"
      },
      onFinished: function (event, currentIndex)
      {
        $(dom.modal_armed).css("display", "flex");
      }
    });

    $("body").find(st.button_add_fiber).on('click', function(){
      var fiber = $("body").find(".js-clothing-fiber-clone").clone();
      fiber.removeClass("js-clothing-fiber-clone");      
      fiber.find(".js-add-fiber").removeClass("js-add-fiber").addClass('js-remove-fiber').text("Eliminar");
      fiber.insertAfter(".js-clothing-fiber-clone");
    }); 

    $(document).on('click', ".js-remove-fiber", function(){
      $(this).parent().parent().remove();
    });

    $(document).on('click', st.button_clothing_add_seals, function(){
      var seals = $("body").find(".js-clothing-seals-clone").clone();
      seals.removeClass("js-clothing-seals-clone");      
      seals.find(".js-clothing-add-seals").removeClass("js-clothing-add-seals").addClass('js-clothing-remove-seals').text("Eliminar");
      seals.insertAfter(".js-clothing-seals-clone");
    });

    $(document).on('click', ".js-clothing-remove-seals", function(){
      $(this).parent().parent().remove();
    });

    $(document).on('click', st.button_clothing_add_country, function(){
      var country = $("body").find(".js-clothing-country-clone").clone();
      country.removeClass("js-clothing-country-clone");      
      country.find(".js-clothing-add-country").removeClass("js-clothing-add-country").addClass('js-clothing-remove-country').text("Eliminar");
      country.insertAfter(".js-clothing-country-clone");
    });

    $(document).on('click', ".js-clothing-remove-country", function(){
      $(this).parent().parent().remove();
    });

        // var form = $(dom.form).show();
        // form.steps({
        //     headerTag: "h3",
        //     bodyTag: "fieldset",
        //     transitionEffect: "slideLeft",
        //     onStepChanging: function (event, currentIndex, newIndex)
        //     {
        //         // Allways allow previous action even if the current form is not valid!
        //         if (currentIndex > newIndex)
        //         {
        //             return true;
        //         }
        //         // Forbid next action on "Warning" step if the user is to young
        //         if (newIndex === 3 && Number($("#age-2").val()) < 18)
        //         {
        //             return false;
        //         }
        //         // Needed in some cases if the user went back (clean up)
        //         if (currentIndex < newIndex)
        //         {
        //             // To remove error styles
        //             form.find(".body:eq(" + newIndex + ") label.error").remove();
        //             form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        //         }
        //         form.validate().settings.ignore = ":disabled,:hidden";
        //         return form.valid();
        //     },
        //     onStepChanged: function (event, currentIndex, priorIndex)
        //     {
        //         // Used to skip the "Warning" step if the user is old enough.
        //         if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
        //         {
        //             form.steps("next");
        //         }
        //         // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
        //         if (currentIndex === 2 && priorIndex === 3)
        //         {
        //             form.steps("previous");
        //         }
        //     },
        //     onFinishing: function (event, currentIndex)
        //     {
        //         form.validate().settings.ignore = ":disabled";
        //         return form.valid();
        //     },
        //     onFinished: function (event, currentIndex)
        //     {
        //         alert("Submitted!");
        //     }
        // }).validate({
        //     errorPlacement: function errorPlacement(error, element) { element.before(error); },
        //     rules: {
        //         confirm: {
        //             equalTo: "#password-2"
        //         }
        //     }
        // });
	};

	var initialize = function() {
    catchDom();
		funcionality();
	};

	return {
		init: initialize
	};

})();
	
module.exports = clothing;
	