var drink = (function() {
		
	var st = {
        form: "#form-drink",
        modal_armed: ".js-armed",
        button_drink_add_seals: ".js-drink-add-seals",
        button_drink_remove_seals: ".js-drink-remove-seals",
        button_drink_add_country: ".js-drink-add-country",
        button_drink_remove_country: ".js-drink-remove-country",
	};

	var dom = {};

	var catchDom = function() {
        dom.form = $(st.form);	
        dom.modal_armed = $(st.modal_armed);	
        dom.button_drink_add_seals = $(st.button_drink_add_seals);		
        dom.button_drink_remove_seals = $(st.button_drink_remove_seals);		
        dom.button_drink_add_country = $(st.button_drink_add_country);    
        dom.button_drink_remove_country = $(st.button_drink_remove_country);				
	};

	function funcionality() {
      $(dom.form).steps({
         headerTag: "h3",
         bodyTag: "section",
         transitionEffect: "fade",
         autoFocus: true,
         labels: {
         next: "Siguiente >",
         finish: "Generar >"
         },
         onFinished: function (event, currentIndex)
         {
            $(dom.modal_armed).css("display", "flex");
         }
      });

      $(document).on('click', st.button_drink_add_seals, function(){
         var seals = $("body").find(".js-drink-seals-clone").clone();
         seals.removeClass("js-drink-seals-clone");      
         seals.find(".js-drink-add-seals").removeClass("js-drink-add-seals").addClass('js-drink-remove-seals').text("Eliminar");
         seals.insertAfter(".js-drink-seals-clone");
      });

      $(document).on('click', ".js-drink-remove-seals", function(){
         $(this).parent().parent().remove();
      });

      $(document).on('click', st.button_drink_add_country, function(){
         var country = $("body").find(".js-drink-country-clone").clone();
         country.removeClass("js-drink-country-clone");      
         country.find(".js-drink-add-country").removeClass("js-drink-add-country").addClass('js-drink-remove-country').text("Eliminar");
         country.insertAfter(".js-drink-country-clone");
       });
   
       $(document).on('click', ".js-drink-remove-country", function(){
         $(this).parent().parent().remove();
       });
	};

	var initialize = function() {
        catchDom();
		funcionality();
	};

	return {
		init: initialize
	};

})();
	
module.exports = drink;
	