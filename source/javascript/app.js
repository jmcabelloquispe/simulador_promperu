var main = require("./modules/main.js");
var faq = require("./modules/faq.js");
var menu = require("./modules/menu.js");
var clothing = require("./modules/clothing.js")
var drink = require("./modules/drink.js")
var shoe = require("./modules/shoe.js")

$(document).on("ready", function() {  
  faq.init();
  menu.init();
  clothing.init();
  drink.init();
  shoe.init();
  main.init();
});